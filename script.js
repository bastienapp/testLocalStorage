document
  .querySelector('#dark-theme')
  .addEventListener('click', function() {
    darkTheme();
  });

document
  .querySelector('#light-theme')
  .addEventListener('click', function() {
    lightTheme();
  });

function darkTheme() {
  const content = document.querySelector('.content');
  content.style.backgroundColor = "#999";
  document.querySelector('.title').style.color = 'white';
  localStorage.setItem("userTheme", "dark");
}

function lightTheme() {
  const content = document.querySelector('.content');
  content.style.backgroundColor = "aliceblue";
  document.querySelector('.title').style.color = 'black';
  localStorage.setItem("userTheme", "light");
}

const themeSelected = localStorage.getItem("userTheme"); // "dark" ou "light"
console.log("themeSelected", themeSelected);

if (themeSelected === "dark") {
  darkTheme();
} else {
  lightTheme();
}

document
  .getElementById('random-value')
  // callback function
  .addEventListener('click', generateRandomValue);

function generateRandomValue() {
  const number = Math.floor(Math.random() * 10) + 1;
  document
    .getElementById('number-value')
    .textContent = number;
  localStorage.setItem("randomNumber", number);
}

const lastValueSaved = localStorage.getItem("randomNumber");
// s'il y a une valeur enregistrée dans le localStorage
if (lastValueSaved != null) {
  // on l'affichage au bon endroit
  document
    .getElementById('number-value')
    .textContent = lastValueSaved;
} else {
  document
    .getElementById('number-value')
    .textContent = "Aucune valeur";
}
